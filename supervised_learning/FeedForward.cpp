// Supervised.cpp: 콘솔 응용 프로그램의 진입점을 정의합니다.

#include "stdafx.h"
#include <iostream>

#define MAX2(a,b) (a) > (b) ? (a) : (b);

using namespace std;

class Neuron
{
public :
	double w_; // weight of one input,
	double b_; // bias

	double getAct(const double &x) {
		// for linear or identity activation functions
		return x;

		// for ReLu activation functions
		//return MAX2(0, 0, x);
	}

	double feedForward(const double &input) {
		// output y = f(sigma), f = activation function
		// sigma = w_ * x + b
		// for multiple inputs,
		// sigma = w0_ * x0_ + w1 * x1_ + .. + b

		const double sigma = w_ * input + b_;
		return getAct(sigma);
	}
};

int main()
{
	Neuron my_neuron;
	my_neuron.w_ = 2.0;
	my_neuron.b_ = 1.0;
	cout << "x = 1.0, y = " << my_neuron.feedForward(1.0) << endl;
	cout << "x = 2.0, y = " << my_neuron.feedForward(2.0) << endl;
	cout << "x = 3.0, y = " << my_neuron.feedForward(3.0) << endl;
	return 0;
}
