
# coding: utf-8

# In[1]:


import gym
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf


# In[2]:


env = gym.make('CartPole-v0')


# In[3]:


input_size = env.observation_space.shape[0]
output_size = env.action_space.n
learning_rate = 0.1


# In[4]:


X = tf.placeholder(shape=[None, input_size], dtype=tf.float32)
W = tf.get_variable("W", shape=[input_size, output_size], initializer=tf.contrib.layers.xavier_initializer())

Qpred = tf.matmul(X, W)
Y = tf.placeholder(shape=[None, output_size], dtype=tf.float32)

loss = tf.reduce_sum(tf.square(Y - Qpred))

train = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss)

init = tf.global_variables_initializer()


# In[5]:


dis = .9
num_episodes = 2000

rList = []


# In[6]:


sess = tf.Session()

sess.run(init)
for i in range(num_episodes):
    s = env.reset()
    e = 1. / ((i / 10) + 1)
    step_count = 0
    done = False

    while not done:
        x = np.reshape(s, [1, input_size])
        Qs = sess.run(Qpred, feed_dict={X: x})
        if np.random.rand(1) < e:
            a = env.action_space.sample()
        else:
            a = np.argmax(Qs)

        s1, reward, done, _ = env.step(a)
        if done:
            Qs[0, a] = -100
        else:
            x1 = np.reshape(s1, [1, input_size])
            Qs1 = sess.run(Qpred, feed_dict={X: x1})
            Qs[0, a] = reward + dis * np.max(Qs1)

        sess.run(train, feed_dict={X: x, Y: Qs})

        step_count += 1
        s = s1
    rList.append(step_count)
    print("Episode: {} steps: {}".format(i, step_count))

    if len(rList) > 10 and np.mean(rList[-100:]) > 195.0:
        break


# In[7]:


observation = env.reset()
reward_sum = 0
while True:
    env.render()

    x = np.reshape(observation, [1, input_size])
    Qs = sess.run(Qpred, feed_dict={X: x})
    a = np.argmax(Qs)

    observation, reward, done, _ = env.step(a)

    reward_sum += reward

    if done:
        print("Total score: {}".format(reward_sum))
        break
        
sess.close()

