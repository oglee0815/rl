import gym
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

env = gym.make('CartPole-v0')

learning_rate = 0.1
input_size = env.observation_space.shape[0]
output_size = env.action_space.n

X = tf.placeholder(tf.float32, [None, input_size], "input_x")

# layer 2로
#W1 = tf.get_variable("W1", [input_size, output_size], initializer=tf.contrib.layers.xavier_initializer())
#Qpred = tf.matmul(X, W1)
h_size = 10

W1 = tf.get_variable("W1", [input_size, h_size], initializer=tf.contrib.layers.xavier_initializer())

layer1 = tf.nn.tanh(tf.matmul(X, W1))

W2 = tf.get_variable("W2", [h_size, output_size], initializer=tf.contrib.layers.xavier_initializer())

Qpred = tf.matmul(layer1, W2)

Y = tf.placeholder(tf.float32, [None, output_size])

loss = tf.reduce_sum(tf.square(Y - Qpred))

train = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss)

num_episodes = 50000
dis = 0.9
rList = []
replay_buffer = []

init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    for i in range(num_episodes):
        e = 1. / ((i / 10) + 1)
        step_count = 0
        state = env.reset()
        done = False

        while not done:
            if np.random.rand(1) < e:
                action = env.action_space.sample()
            else:
                x = np.reshape(state, [1, input_size])
                Qs = sess.run(Qpred, feed_dict={X: x})
                action = np.argmax(Qs)

            new_state, reward, done, _ = env.step(action)
            step_count += 1

            replay_buffer.append([state, action, reward, new_state, done, step_count])
            if len(replay_buffer) > 50000:
                replay_buffer.pop(0)

            state = new_state

        # episode 10회 돌때마다 샘플링 해서 학습시킨다.
        if i % 10 == 1:
            x_array = []
            y_array = []
            for _ in range(650):
                if len(replay_buffer) == 0:
                    break
                s, a, r, s1, dn, sc = replay_buffer.pop(np.random.randint(0, len(replay_buffer), 1)[0])

                x = np.reshape(s, [1, input_size])
                Qs = sess.run(Qpred, feed_dict={X: x})

                if dn and sc < 200:
                    Qs[0, a] = -100
                elif dn and sc == 200:
                    continue
                else:
                    x1 = np.reshape(s1, [1, input_size])
                    Qs1 = sess.run(Qpred, feed_dict={X: x1})
                    Qs[0, a] = r + dis * np.max(Qs1)
                x_array.append(x)
                y_array.append(Qs)

            for idx, xx in enumerate(x_array):
                yy = y_array[idx]
                sess.run(train, feed_dict={X: xx, Y: yy})

        rList.append(step_count)
        print("Episode: {} steps : {}".format(i, step_count))
        # if len(rList) > 10 and np.mean(rList[-10:]) >= 200:
        #     break

    plt.bar(range(len(rList)), rList, color="blue")
    plt.show()

    # 학습된 결과를 본다.
    observation = env.reset()

    done = False
    totReward = 0

    while not done:
        env.render()

        x = np.reshape(observation, [1, input_size])
        Qs = sess.run(Qpred, feed_dict={X: x})

        action = np.argmax(Qs)

        new_observation, reward, done, _ = env.step(action)

        totReward += reward
        observation = new_observation

    print(totReward)
